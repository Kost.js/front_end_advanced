const pandemicStartMap = "X01000000X000X011X0X";

document.getElementById('app').innerHTML = showMap(pandemicStartMap); 

function showMap(startMap) {
    const finalMap = startMap.split('X').map((el) => el.includes('1') ? Array(el.length).fill('1').join('') : el).join('X');
    const total = [...startMap].filter(el => !isNaN(el)).length;
    const infected = [...finalMap].filter(Number).length;

    return `
      <h2>Pandemic Start</h2>
      ${injestCells(startMap)}
      <h2>Pandemic Stop</h2>
      ${injestCells(finalMap)}
      <p class="result">Total: ${total}</p>
      <p class="result">Infected: ${infected}</p>
      <p class="result">Percentage: ${(total / infected) * 100}&#37;</p>
    `;
}

function injestCells(data) {
    const styles = {
      'X': 'ocean',
      '0': 'uninfected',
      '1': 'infected'
    };
    
    return `
        <div>
          ${[...data].map(el => `<div class="square ${styles[el]}"></div>`).join('')}
        </div>
    `;
}

